package vn.controller;

import org.springframework.web.bind.annotation.*;
import vn.base.BaseController;
import vn.util.GoogleUtil;

import java.util.Base64;

/**
 * Created by nhanvt on 2020/07/08
 */
@RestController
@RequestMapping(value = "/media", method = RequestMethod.GET)
public class MediaController extends BaseController {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String GETMedia() {
        return "Hello";
    }

    @GetMapping(value = "/test")
    public void speech(@RequestParam(value = "text") String text) {
        try {
            GoogleUtil googleUtil = new GoogleUtil();
            byte[] decodedBytes = Base64.getDecoder().decode(googleUtil.textToSpeech(text).getAudioContent());
            String decodedString = new String(decodedBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
