package vn.bo;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by nhanvt on 2020/06/09
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RequestBody {
    @SerializedName("user_id")
    private Long id;
    @SerializedName("request_time")
    private Long registed;
    @SerializedName("shared_key")
    private String sharedKey;
}
