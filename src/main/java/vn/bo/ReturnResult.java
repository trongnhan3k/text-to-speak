package vn.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import okhttp3.ResponseBody;

import java.util.Map;

/**
 * Created by nhanvt on 2020/06/17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReturnResult {
    private String response;
    private Map<String, String> headerMap;
    private Integer status;

}
