package vn.util.config;

import vn.util.ConfigurationManager;

/**
 * Created by nhanvt on 2020/06/11
 */
public class Constant {
    public static int SERVICE_PORT = ConfigurationManager.getInstance().getInt("server.port");

    public static String MYSQL_URL = ConfigurationManager.getInstance().getString("mysql.url");
    public static String MYSQL_USER = ConfigurationManager.getInstance().getString("mysql.user");
    public static String MYSQL_PASSWORD = ConfigurationManager.getInstance().getString("mysql.pass");
    public static int MYSQL_POOL_INITIAL_SIZE = ConfigurationManager.getInstance().getInt("mysql.pool.initial.size");
    public static int MYSQL_POOL_MIN_IDLE = ConfigurationManager.getInstance().getInt("mysql.pool.min.idle");
    public static int MYSQL_POOL_MAX_IDLE = ConfigurationManager.getInstance().getInt("mysql.pool.max.idle");
    public static String TABLE_CLIENT_MANAGER = ConfigurationManager.getInstance().getString("mysql.table.management.client");
    public static String TABLE_API_MANAGER = ConfigurationManager.getInstance().getString("mysql.table.management.api");
    public static String TABLE_BLACKLIST_MANAGER = ConfigurationManager.getInstance().getString("mysql.table.management.blacklist");
    public static String TEMP_PATH = ConfigurationManager.getInstance().getString("tmp");

}
