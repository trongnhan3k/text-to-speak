package vn.util;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

@Component
public class FileUtil {

    public void write(MultipartFile file, String dir) {
        File tmp = new File(dir);

        try (OutputStream os = new FileOutputStream(tmp)) {
            os.write(file.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}