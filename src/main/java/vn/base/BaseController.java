package vn.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vn.global.GlobalObject;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nhanvt on 2020/06/09
 */
public abstract class BaseController extends Base {

    protected static final Logger logger = LoggerFactory.getLogger("RequestLog");

    protected String getResponse(Object baseReponse) {
        return GlobalObject.gsonNull.toJson(baseReponse);
    }

    protected String getRequestParam(HttpServletRequest request){
        String fullUri = request.getRequestURI();
        Map<String, String[]> mapBody = new HashMap<>();
        if (request.getParameterMap() != null) {
            mapBody = request.getParameterMap();
        }
        if (mapBody.size() != 0) {
            fullUri += "?";
            for (Map.Entry<String, String[]> entry : mapBody.entrySet()) {
                for (int i = 0; i < entry.getValue().length; i++) {
                    fullUri += entry.getKey() + "=" + entry.getValue()[i] + "&";
                }
            }
            fullUri = fullUri.substring(0, fullUri.length() - 1);
        }
        return fullUri;
    }
}
