package vn;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import vn.base.Base;
import vn.task.CheckTask;
import vn.util.config.Constant;

import java.lang.management.ManagementFactory;
import java.util.Properties;

/**
 * Created by nhanvt on 2020/06/09
 */
@SpringBootApplication
public class TextToSpeechMain extends Base {
    public static void main(String[] args) {
        Properties pros = new Properties();
        pros.put("server.port", Constant.SERVICE_PORT);

        SpringApplicationBuilder applicationBuilder = new SpringApplicationBuilder()
                .sources(TextToSpeechMain.class)
                .properties(pros);
        applicationBuilder.run(args);

        logger.info("Service start at port {}\t{}", Constant.SERVICE_PORT, ManagementFactory.getRuntimeMXBean().getName());

        check();
    }

    private static void check() {
        CheckTask checkTask = new CheckTask();
        Thread checkThred = new Thread(checkTask, "th_check");
        checkThred.start();
    }
}

